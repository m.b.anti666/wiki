## Introduction

In order to get anything out of the conversations on IRC, it's useful to understand some of the basic terms that relate to nouveau.  Unlike the terms in [[DRI Explanation|http://people.freedesktop.org/~ajax/dri-explanation.txt]], these terms are more-or-less specific to the nouveau project. 


## Terms

* bo - buffer object. A block of memory. Can store a texture, a render target, a pixmap, shader code, vertex buffer... everything. 
* CB - [[Const Buffer|http://wiki.github.com/pathscale/pscnv/nvidia_compute]] 
* CUDA - [[CUDA|http://en.wikipedia.org/wiki/CUDA]], [[CUDA|http://www.nvidia.com/object/cuda_develop.html]], [[CUDA|http://wiki.github.com/pathscale/pscnv/nvidia_compute]] 
* DAC - [[DAC|http://en.wikipedia.org/wiki/Digital-to-analog_converter]] 
* DCB - Display Control Block 
* EXA - [[EXA|http://en.wikipedia.org/wiki/EXA]] 
* fence - Piece of memory which is updated by GPU when it reaches some step in command stream 
* GART - [[GART|http://en.wikipedia.org/wiki/Graphics_Address_Remapping_Table]] 
* LUT - Lookup table. Usually used to refer to the color palette in indexed modes. 
* M2MF - memory to memory format. An object used to copy blocks of memory. 
* MP - [[CUDA|http://wiki.github.com/pathscale/pscnv/nvidia_compute#thread-hierarchy]] 
* MSAA - [[MSAA|http://en.wikipedia.org/wiki/Multisample_anti-aliasing]] 
* PCI memory - An area of system memory that can be accessed by direct-memory access from the GPU 
* PFIFO engine -  The engine of the GPU that reads commands from a section of memory and relays them to the PGRAPH engine 
* PGRAPH engine - The engine of the GPU that actually performs graphics operations like blitting and drawing triangles.  It can be programmed by directly writing to its registers in MMIO space or by feeding commands through the PFIFO engine. 
* pitch - length of one line in bytes 
* PMISC 
* PRAMIN - instance memory area 
* PUPLOAD 
* PVIDEO engine - The engine of the GPU that controls the video overlay.  Only NV10 - NV40 have this hardware.  Later cards acheive a similar effects with video textures and the 2D blitter of the PGRAPH engine. 
* Push Buffer - The area of system memory where the commands for the FIFO engine are stored 
* relocation 
* ring 
* ROP - raster operation 
* SIFC - Streched Image From CPU. Used to upload all kinds of data to GPU memory. 
* SOR 
* surface - basically, a 2d array of pixels. Can be texture, render target, or pixmap, or... 
* TCL - Transform, Clipping, Lighting. Fixed-function vertex processing, first implemented in hardware in NV10, later made obsolete by vertex shaders. 
* TIC - Texture Image Control. A memory block containing texture address, dimension, format, etc. on NV50. 
* TMDS - Transition Minimized Differential Signaling [[TMDS|http://en.wikipedia.org/wiki/Transition Minimized Differential Signaling]] 
* TOR - value which is OR'ed with data when relocation points to BO in GART 
* TP - [[CUDA tile processor|http://wiki.github.com/pathscale/pscnv/nvidia_compute]] 
* TSC - Texture Sampler Control. A memory block containing texture wrap modes, border setup, etc. on NV50. 
* VBIOS - [[Video BIOS|http://en.wikipedia.org/wiki/Video BIOS]] 
* VOR - value which is OR'ed with data when relocation points to BO in VRAM 
* VRAM - [[Video RAM|http://en.wikipedia.org/wiki/VRAM]] 
* Xv - X video extension [[Xv|http://en.wikipedia.org/wiki/X_video_extension]] 
