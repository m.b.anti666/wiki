## Informe irregular sobre el Desarrollo de Nouveau


## Edición del 7 de julio


### Introducción

Hola de nuevo. Este es el número 23 de nuestro TiNDC. De nuevo queremos ofreceros una visión de nuestro trabajo. 

Antes de empezar, una pequeña corrección/clarificación sobre lo que escribimos en el último número: 

* "Las tarjetas NVidia ovrecen un objeto de memoria PCI y otro de memoria AGP. [...] El objeto PCI y AGP difieren en un aspecto principal: el objeto PCI apunta a una lista de páginas asignadas, mientras que el  AGP apunta a una dirección y tiene un tamaño." 
Esto no es totalmente correcto. Una descripción mejor habría sido: 

* Los objetos PCI y AGP difieren únicamente en un aspecto principal, establecen los bits necesarios para indicar "por favor, realiza una transferencia PCI DMA" o "haz una transferencia AGP DMA". Ambos tienen listas de páginas (incluso el objeto de video ram puede tener una). Es posible incluso usar un puntero a un bloque continuo de memoria con el objeto PCI. Sin embargo, no es fácil crear ese bloque para PCI, y usar una lista de páginas con un objeto AGP haría perder las ventajas de usar la apertura AGP... (Gracias a darktama por señalar estas cuestiones). 
Tras un tiempo, Phoronix.com ofreció un artículo sobre el "estado de Nouveau", en el que describe el estado actual del proyecto y realizan algunas pruebas con FC usando varias tarjetas (chipsets móviles, 6600GT y 8500 GT). [[http://www.phoronix.com/scan.php?page=article&item=765&num=1|http://www.phoronix.com/scan.php?page=article&item=765&num=1]] En el foro, airlied contestó una pregunta sobre el soporte de randr1.2. En estos momentos se ha detenido el trabajo debido a varios motivos, pero espera volver a la actividad en septiembre. 

Por favor, advertid que el artículo tiene dos pegas: 1. hace las pruebas en Fedora Core con el paquete suministrado por la distribución, que es muy antiguo según indica ajax, el mantenedor del paquete. 2. Afirma que glxgears funciona, y muestra un pantallazo. Desgraciadamente, el pantallazo prueba que la tarjeta no está usando la aceleración, sino una renderización por software (MESA). 

Ya que no hay más cuestiones relevantes que comentar en esta sección por el momento... avancemos al... 


### ... Estado Actual

Tras las actualizaciones de Darktama del DRM, pmdata advirtió bloqueos. Ambos intentaron localizar las razones de estos, y, poco después de publicar el último número, darktama localizó también algunos problemas en el código NV1x, y proporcionó a pmdata un parche para que lo probase. Pmdata informó un día más tarde de que funcionaba correctamente. 

Una vez eliminado este problema, darktama subió todos sus cambios al repositorio el viernes 29, y, al romper finalemente la API, la diversión fue abundante. 

pmdata todavía pudo encontrar errores PGRAPH al ejecutar glxgears en su NV15 pero parecían estar relacionados a una inicialización incorrecta del contexto de la tarjeta. Tras analizar un volcado MMIO de la tarjeta se dio cuenta de que el controlador debe esperar a que finalice la operación actual antes de que se puedan realizar cambios de contexto. Creó un pequeño parche que pudo ser probado con éxito ([[http://people.freedesktop.org/~pmandin|http://people.freedesktop.org/~pmandin]]). El parche debería corregirse un poco antes de integrarlo definitivamente, ya que podría, en teoría, causar bloqueos. 

[[!img http://www.ping.de/sites/koala/TiNDC_23/glxgears-20070704.png] 

Finalmente, NV4x implementa las funciones EXA a través del sistema 3D, lo que debería: 

* a) eliminar algunos de los errores de EXA antiguos que ya tratamos en números anteriores. 
* b) darle al conjunto un buen incremento de velocidad. 
Técnicamente, pudimos ver algunas mejoras, como el uso de notificadores para la finalización de las transferencias de DMA y una función un poco mejorada de [[UploadToScreen|UploadToScreen]] (EXA). 

EXA también funcionará para el resto de tarjetas, pero usando fundamentalmente implementaciones software y sin aceleración por hardware. Para las tarjetas NV3x no debería hacer falta mucho para acelerar EXA, aunque las tarjetas más antiguas necesitarán su propia implementación. 

La parte mala de estos parches es que incluso el funcionamiento en 2D de las tarjetas NV41, NV42, NV44,NV45, NV47,NV48 y NV4C puede fallar, debido a que faltan los códigos mágicos (microcódigo) para la inicialización. En ese caso, por favor, informad de ello en #nouveau y preparaos para realizar un trazado de MMioTrace. 

Justo cuando pensábamos que EXA era ya otro subsistema que habíamos introducido sin errores (*ahem*), AndrewR apareció con el informe de fallos 11425 ([[http://bugs.freedesktop.org/show_bug.cgi?id=11425|http://bugs.freedesktop.org/show_bug.cgi?id=11425]]). Otros informes adicionales hicieron que Darktama preparase un conjunto adicional de parches que solucionaron todos los problemas de visualización conocidos hasta ese momento. Las informaciones posteriores sobre los cambios han sido positivas. 

El primer parche de Ahuillet hizo funcionar Xv para AGP con DMA, y la mayor parte de la gente que lo probó lo hizo con éxito. El consumo CPU parece reducirse, en el mejor caso, del 80% (Xorg y mplayer combinados) al 20%. Este caso es extremo, pero se ha visto como todos los casos probados han mejorado. 

Tras regresar de su operación dental, ahuillet volvió convencido para lograr hacer funcionar el PCI DMA. Su travesía tuvo el auxilio de Airlied, darktama y marcheu, quienes le dieron la información que necesitaba para realizar y comprender la tarea que tenía entre manos. Sin embargo, sus primeros intentos condujeron a bloqueos en su tarjeta. 

Los primeros intentos estuvieron marcados por errores de DRM debido al uso de parámetros incorrectos de reserva de objetos DMA (allocation). Posteriormente, las X se colgaban o bloqueaban aunque el sistema seguía en funcionamiento. Tras un trabajo de diagnóstico con Marcheu, parece que el espacio de direcciones del kernel para PCI no se trasladaban correctamente a direcciones del espacio de usuario. Por lo que ambos espacios, de kernel y usuario, usaban distintas direcciones y llevaban a la confusión. Tras sus cambios, el DMA de PCI comenzó a funcionar, tal como podemos ver en estas captura de pantalla: 

[[!img http://www.ping.de/sites/koala/TiNDC_23/pci_dma_Xv_1.png] [[!img http://www.ping.de/sites/koala/TiNDC_23/pci_dma_Xv_2.png] 

Esta versión está usando el blitter (si no veríamos únicamente un rectángulo azul en lugar de la película). Si se compara el rendimiento de Xv en nouveau al rendimiento en nv, nouveau parece ser mucho más lento. A la izquierda la primera versión que funciónó y a la derecha el resultado unas horas más tarde. 

Lo siguiente en la lista de tareas de ahuillet es: comparar el rendimiento de nv y nouveau respecto a PCI / AGP DMA además del rendimiento de blitter / overlay. 

Esta investigación no se realizó hasta el 05.07.07 , y mostró que lo que era mucho más lento eran las transferencias de DMA! AGP y PCI DMA hacían Xv mucho más lento que las copias de CPU. El intercalado de blitting y DMA (para el siguiente frame) no conseguía una mejora apreciable, pero deshacerse de la habilitación de un buffer temporal parecía mejorar un poco las cosas. Se puede obtener una visión más ámplia del asunto aquí: [[http://people.freedesktop.org/~ahuillet/xv_benchmarks.pdf|http://people.freedesktop.org/~ahuillet/xv_benchmarks.pdf]] Aún así, incluso el mejor caso es todavía mucho más lento que la versión de nv. Ahuillet mostró su desánimo, pero inició más investigaciones. 

Los parches de Darktama al DRM y DDX (rama nv50) fueron probados en otra G84 por KoalaBR. Funcionaron, aunque muy lentamente, tanto en el modo por defecto como en el "[[MigrationHeuristic|MigrationHeuristic]] greedy". 

Ahora la utilidad createdump.sh comprueba si existe libSDL en el sistema de trabajo. Sin embargo, pq se quejó con razón de que el test usase locate (posiblemente desfasado, o no disponible) y sobre qué comprobaba (la existencia de libSDL.so). Se cambió el script para que detectase sdl-config. 

Con airlied de mudanza a Brisbane y alejado de su teclado, Matthew Garret apareció en escena y envió un par de parches para mejorar un poco el estado de la rama randr12. Los parches pueden encontrarse aquí, ya que todavía no han sido integrados en el repositorio principal: [[http://lists.freedesktop.org/archives/nouveau/2007-July/000179.html|http://lists.freedesktop.org/archives/nouveau/2007-July/000179.html]] 

Sintiéndose valiente, y con un segundo glxgears en funcionamiento, pq tuvo un bloqueo del sistema (sin ES de serie, ni SYSRQ, ni ssh, simplemente muerto). Puesto que esto funcionaba en enero (con basura estática en la ventana), todavía hay mucho trabajo de rastreo y depuración que hacer. Lo próxima en la agenda de pq era decodificar los mensajes de error del DRM a una forma legible. Un día más tarde lo consiguió. 

Todavía no tenemos noticias de la Software Freedom Conservancy. 


### Ayuda necesaria

Como mencionamos anteriormente, necesitamos trazas de MMioTrace para las tarjetas NV41, NV42, NV44,NV45, NV47,NV48  y NV4C. Por favor, haceos notar en el canal. 

Si no os importa, probad también los parches de ahuillet en git://people.freedesktop.org/~ahuillet/xf86-video-nouveau e informadle de cómo os funcionan. Sin embargo, estad preparados para encontrar problemas, disfuncionalidades y bloqueos, ya que ¡se trata de un trabajo en marcha! 
