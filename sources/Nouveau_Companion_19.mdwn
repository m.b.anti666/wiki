[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/EN/[[ES|Nouveau_Companion_19-es]]/[[FR|Nouveau_Companion_19-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## The irregular Nouveau-Development companion


## Issue for May, 12th


### Intro

Hello again to issue number 19 of the TiNDC.  

Seems as if the TTM manager has been posted to the Linux Kernel Mailing list for review. If all goes well, it will get included into the kernel. Unfortunately for us, the TTM seems to expect exactly one FIFO in kernel space while nouveau has at least 16 FIFOs in user space.  So now what? 

* we will require changes to the core TTM code. 
* we will need to implement the nouveau driver code to interface with TTM. 
* we will need to implement userspace code to use the TTM. 
By the way, included in the TTM patch is mode setting in kernel space too. We  have already covered that topic in an earlier TiNDC. 

The review was mostly positive, but some changes are needed in the area of ioctl(), coding style and documentation. The code is back in the land of Xorg / DRM hackers and will get some overhaul and then go back to the kernel list for review. Well, seems as if the guys at #dri-devel want a somewhat more extensive redesign (Seems as if Intel wants more than 1 FIFO too). All in all it seems that some more delay is looming on the horizon. 

Our Vacation of Code project will start in the beginning of June and last until August. Ahuillet, who is trying to improve our xv support, has agreed to give us a status report for each TiNDC issue which will be published during that time frame. Thanks ahuillet!  

Well it seems as if Nvidia put the XvMC feature for G8x based cards on the backburner or may even forfeit the implementation. Could be our first  "nouveau only feature". 

Currently progress and work is somewhat slow, as both marcheu and darktama are quite occupied with real life things. Starting next month however, things should hopefully speed up again.  

We are still waiting for a final decision on the acceptance of our project into the [[Software Freedom Conservancy|http://conservancy.softwarefreedom.org/]]. Therefore our developers are still on their own regarding hardware purchases but slowly G8x based cards are invading our ranks :)  Don't hold your breath for a working driver though, as explained later on, there is much work to do. 


### Current status

In the last issue we noted that the port to BSD was blocked due to the DRM lacking some important features. Even before the last issue was published, camje_lemon tried his luck to get nouveau working. Shortly after publishing the last TiNDC he claimed success. So all you *BSD fans rejoice (and help us porting and testing). 

jwstolk did further work on the macro finder. After stomping out bugs galore, he asked pq and z3ro for testing. PQ had some suggestions on how to improve on the then current design, which jwstolk took into consideration. Marcheu did like the idea. 

Ahuillet started preparations for his Vacation of Code project, sponsered by Xorg. He had a look at the code and claimed total ignorance about the issues at hand. However he asked some specific questions regarding the code later on which put his ignorance really in doubt. 

We do have a mailing list [[here|http://lists.freedesktop.org/mailman/listinfo/nouveau]]. You mail mail us bugs and everything nouveau related there. CJB was insistent enough to get this going. Still, our main focus for now will be the IRC channel. Nevertheless, please use it. 

The problems on NV15 cards is finally solved: When trying to emit 3D rendering commands (e.g. running glxgears) the machine locked up hard. It was caused by a interrupt storm tearing down the whole system. After  some prodding matc found the error: We initialized the context incorrectly. Instead of a size of 32 bit we did init 64 bit. Abotezatu volunteered to check this patch out and reported success. A few days later pmdata tried his luck too and he too reported success, but now X crashes for him when "running" glxgears and clicking outside the glxgears window. 

pq and z3ro continued finalizing their specification of the rules-ng database. We now have documentation tags available. Further work will go into creating the needed tools which can work with this specs. Most of the discussion move to #dri-devel though, as the radeon folks is trying to use rules-ng for their hardware too. 

Darktama finally bit the bullet (Bon Appetite) and bought a new system including NVidia 8600 GT. A first inspection of mmio traces revealed that the 8600 cards are very different from those produced earlier. And when Wallbraker offered dumps from his new and shiny 8800 GTS it was revealed that even between G80 and G84 there are some slight differences. 

It seems that either the current beta driver has problems with some vertex programs (e.g. vertex_program3() in renouveau) or we can't code vertex programs correctly. The result is that renouveau crashes when running vertex_program3(). You can safely comment this test out, if renouveau crashes on your G8x card. 

Currently, nothing works in regards to these cards. 2D will see some work first. 3D is a different kind of story. We are quite sure that we can either do it corerctly and figure out how the new cards work or we can cheat our way around these issue and use a kind of compability mode (with NV4x) which  will yield results sooner but will probably be a dead end when looking for  future development of our driver. 

Again some more development on the Randr1.2 branch. Airlied added some new setup code, removed some unused code and hopefully fixed some bugs. 


### Help needed

Please send in renouveau dumps for SLI and 8x00. Unfortunately we still have no current list available, but rest assured that your dumps won't get lost. 

Furthermore we are looking for MMio dumps of cards which currently do not run glxgears correctly. 

If you have a NV15, please do test our latest changes. 

[[<<< Previous Issue|Nouveau_Companion_18]] | [[Next Issue >>>|Nouveau_Companion_20]] 
