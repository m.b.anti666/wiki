[[!toc startlevel=1 levels=2]]

# Why use performance counters?

Performance counters are really useful in order to find bottlenecks in real applications. They report very accurate metrics regarding how the GPU is idle/busy.

# How to use performance counters?

## GALLIUM_HUD

GALLIUM_HUD is an environment variable in mesa which can be used to monitor performance counters with a nice interface.


To get the list of available queries (which can be different regarding your chipset), run 'GALLIUM_HUD="help" glxgears'.


Once you have that list, select which queries you want to monitor, for example 'inst_executed' and start monitoring with 'GALLIUM_HUD="inst_executed" glxgears'.


## apitrace

apitrace is tool for tracing graphics APIs like OpenGL, but it can also be used to replay a trace and monitor perf counters per frames or per draw-calls.


To get the list of available queries, run 'glretrace --list-metrics <my.trace>'. You will see different backends like GL_AMD_performance_monitor and opengl. In each backends, you can have different groups of queries, like "MP counters" and "Performance metrics".


Once you have that list, select which queries you want to monitor before replaying the trace and run 'glretrace --pframes=GL_AMD_performance_monitor:inst_executed' for example.


# Global perf counters

These performance counters are global. They are configured through PCOUNTER by writing values directly to MMIO. The kernelspace interface is already merged but the userspace one is still WIP.

## Status

[[!table header="no" class="ColorTable" data="""
 **Hardware events** | **[[NV50]]**
geom_primitive_in_count      | [[!color background=orange text=WIP ]]
geom_primitive_out_count     | [[!color background=orange text=WIP ]]
geom_vertex_in_count         | [[!color background=orange text=WIP ]]
geom_vertex_out_count        | [[!color background=orange text=WIP ]]
gld_128b                     | [[!color background=orange text=WIP ]]
gld_32b                      | [[!color background=orange text=WIP ]]
gld_64b                      | [[!color background=orange text=WIP ]]
gld_coherent                 | [[!color background=orange text=WIP ]]
gld_incoherent               | [[!color background=orange text=WIP ]]
gld_request                  | [[!color background=orange text=WIP ]]
gld_total                    | [[!color background=orange text=WIP ]]
gpu_idle                     | [[!color background=orange text=WIP ]]
gst_128b                     | [[!color background=orange text=WIP ]]
gst_32b                      | [[!color background=orange text=WIP ]]
gst_64b                      | [[!color background=orange text=WIP ]]
gst_coherent                 | [[!color background=orange text=WIP ]]
gst_incoherent               | [[!color background=orange text=WIP ]]
gst_request                  | [[!color background=orange text=WIP ]]
gst_total                    | [[!color background=orange text=WIP ]]
input_asembler_waits_for_fb  | [[!color background=orange text=WIP ]]
input_assembler_busy         | [[!color background=orange text=WIP ]]
local_load                   | [[!color background=orange text=WIP ]]
local_store                  | [[!color background=orange text=WIP ]]
rasterizer_tiles_in_count              | [[!color background=orange text=WIP ]]
rasterizer_tiles_killed_by_zcull_count | [[!color background=orange text=WIP ]]
rop_busy                               | [[!color background=orange text=WIP ]]
rop_samples_killed_by_earlyz_count     | [[!color background=orange text=WIP ]]
rop_samples_killed_by_latez_count      | [[!color background=orange text=WIP ]]
rop_waits_for_fb                       | [[!color background=orange text=WIP ]]
rop_waits_for_shader                   | [[!color background=orange text=WIP ]]
setup_line_count                       | [[!color background=orange text=WIP ]]
setup_point_count                      | [[!color background=orange text=WIP ]]
setup_primitive_count                  | [[!color background=orange text=WIP ]]
setup_primitive_culled_count           | [[!color background=orange text=WIP ]]
setup_triangle_count                   | [[!color background=orange text=WIP ]]
stream_out_busy                        | [[!color background=orange text=WIP ]]
tex_cache_hit                          | [[!color background=orange text=WIP ]]
tex_cache_miss                         | [[!color background=orange text=WIP ]]
tex_waits_for_fb                       | [[!color background=orange text=WIP ]]
vertex_attribute_count                 | [[!color background=orange text=WIP ]]
"""]]

# MP perf counters

These performance counters are per-context.
They are configured through the command stream and we use a compute shader to read back the values (ie. $pm0..$pm7 sregs).

## Status

[[!table header="no" class="ColorTable" data="""
 **Hardware events** | **[[SM20]]**<sup>1</sup><sup>2</sup> | **[[SM21]]**<sup>2</sup> | **[[SM30]]** | **[[SM35]]** | **[[SM50]]**

active_cycles                           | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
active_ctas                             | N/A | N/A | N/A | N/A  | [[!color background=palegreen text=DONE ]]
active_warps                            | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=orange text=WIP ]]     | [[!color background=orange text=WIP ]] | [[!color background=palegreen text=DONE ]]
atom_cas_count                          | N/A | N/A  | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
atom_count                              | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
branch/divergent_branch                 | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A | [[!color background=palegreen text=DONE ]]
{gld,gst}\_request                      | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
global_atom_cas                         | N/A | N/A | N/A | N/A  | [[!color background=palegreen text=DONE ]]
global_{load,store}                     | N/A | N/A | N/A | N/A  | [[!color background=palegreen text=DONE ]]
global_{ld,st}\_mem_divergence_replays  | N/A | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
global_store_transaction                | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
gred_count                              | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
inst_executed                           | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
inst_issued (and variants)              | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
l1_global_load_{hit,miss}               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
\__l1_global_{load,store}\_transactions | N/A | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
l1_local_{load,store}\_{hit,miss}       | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
l1_shared_{load,store}\_transactions    | N/A | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
local_{load,store}                      | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
local_{load,store}\_transactions        | N/A | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
not_predicated_off_thread_inst_executed | N/A | N/A | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
prof_trigger_{00-07}                    | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
shared_atom                             | N/A | N/A | N/A | N/A  | [[!color background=palegreen text=DONE ]]
shared_atom_cas                         | N/A | N/A | N/A | N/A  | [[!color background=palegreen text=DONE ]]
shared_{ld,st}\_transactions            | N/A | N/A | N/A | N/A  | [[!color background=palegreen text=DONE ]]
shared_{load,store}                     | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
shared_{load,store}\_bank_conflict      | N/A | N/A | N/A | N/A | [[!color background=palegreen text=DONE ]]
shared_{load,store}\_replay             | N/A | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
sm_cta_launched                         | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]| [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
thread_inst_executed (and variants)     | [[!color background=palegreen text=DONE ]]| [[!color background=palegreen text=DONE ]] | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
threads_launched                        | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
uncached_global_load_transaction        | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A
warps_launched                          | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]


"""]]

## Notes

<sup>1</sup> MP perf counters on GF100/GF110 (SM20) are buggy because we have a context-switch problem that needs to be fixed. Results might be wrong, be careful!

<sup>2</sup> TODO means those perf counters are exposed through PCOUNTER.

# Metrics
## Status

[[!table header="no" class="ColorTable" data="""
 **Name** | **[[SM20]]** | **[[SM21]]** | **[[SM30]]** | **[[SM35]]** | **[[SM50]]**
achieved_occupancy                     | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
alu_fu_utilization                     | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
atomic_replay_overhead                 | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
atomic_throughput                      | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
atomic_transactions                    | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
atomic_transactions_per_request        | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
branch_efficiency                      | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | N/A | [[!color background=palegreen text=DONE ]]
cf_executed                            | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
cf_fu_utilization                      | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
cf_issued                              | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
dram_read_throughput                   | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
dram_read_transactions                 | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
dram_utilization                       | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
dram_write_throughput                  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
dram_write_transactions                | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
eligible_warps_per_cycle               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_count_dp                          | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_count_d                           | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_count_dp_fma                      | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_count_dp_mul                      | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_count_sp                          | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_count_sp_add                      | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_count_sp_fma                      | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_count_sp_mul                      | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_count_sp_special                  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_dp_efficiency                     | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
flop_sp_efficiency                     | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
gld_efficiency                         | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
gld_requested_throughput               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
gld_throughput                         | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
gld_transactions                       | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
gld_transactions_per_request           | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
global_cache_replay_overhead           | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
gst_efficiency                         | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
gst_requested_throughput               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
gst_throughput                         | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
gst_transactions                       | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
gst_transactions_per_request           | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
inst_bit_convert                       | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
inst_compute_ld_st                     | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
inst_control                           | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
inst_executed                          | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
inst_fp_32                             | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
inst_fp_64                             | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
inst_integer                           | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
inst_inter_thread_communication        | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
inst_issued                            | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
inst_misc                              | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
inst_per_warp                          | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
inst_replay_overhead                   | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
ipc                                    | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
issued_ipc                             | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
issue_slots                            | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
issue_slot_utilization                 | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
l1_cache_global_hit_rate               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l1_cache_local_hit_rate                | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l1_shared_utilization                  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_atomic_throughput                   | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_atomic_transactions                 | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_l1_read_hit_rate                    | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_l1_read_throughput                  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_l1_read_transactions                | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_l1_write_throughput                 | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_l1_write_transactions               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_read_throughput                     | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_read_transactions                   | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_tex_read_transactions               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_texture_read_hit_rate               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_texture_read_throughput             | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_utilization                         | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_write_throughput                    | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
l2_write_transactions                  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
ldst_executed                          | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
ldst_fu_utilization                    | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
ldst_issued                            | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
local_load_throughput                  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
local_load_transactions                | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
local_load_transactions_per_request    | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
local_memory_overhead                  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
local_replay_overhead                  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
local_store_throughput                 | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
local_store_transactions               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
local_store_transactions_per_request   | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
shared_efficiency                      | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
shared_load_throughput                 | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
shared_load_transactions               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
shared_load_transactions_per_request   | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
shared_replay_overhead                 | N/A | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE]] | [[!color background=red text=TODO ]]
shared_store_throughput                | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
shared_store_transactions              | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
shared_store_transactions_per_request  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
sm_efficiency                          | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
stall_exec_dependency                  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
stall_inst_fetch                       | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
stall_memory_dependency                | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
stall_memory_throttle                  | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
stall_other                            | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
stall_pipe_busy                        | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
stall_sync                             | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
stall_texture                          | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
sysmem_read_throughput                 | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
sysmem_read_transactions               | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
sysmem_utilization                     | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
sysmem_write_throughput                | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
sysmem_write_transactions              | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
tex_cache_hit_rate                     | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
tex_cache_throughput                   | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
tex_cache_transactions                 | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
tex_fu_utilization                     | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
tex_utilization                        | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]]
warp_execution_efficiency              | [[!color background=red text=TODO ]] | [[!color background=red text=TODO ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
warp_nonpred_execution_efficiency              | N/A | N/A | N/A | [[!color background=palegreen text=DONE ]] | [[!color background=palegreen text=DONE ]]
"""]]
